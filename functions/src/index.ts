import * as functions from 'firebase-functions';

//const adminFireStore = require('firebase-admin').initializeApp();

const adminRTDB = require('firebase-admin');
adminRTDB.initializeApp({
    databaseURL: 'https://spammetest-48305.firebaseio.com/'
});

//const db = admin.firestore();

//const firebaseAdmin = admin;

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//


export const SendMessage 
    = functions.firestore.document('user/{userId}/messages/{messageId}')
    .onCreate(async(snap,context)=>{
        const newData = snap.data();
        console.log(newData);
        
        const msgBody = newData && newData.content;
        const msgRecipient = newData && newData.recipient;
        adminRTDB.database().ref(`fcmTokens/${msgRecipient}`).on('value',function(snapshot:any){
            console.log('snapshot', snapshot);
            console.log(snapshot.val());
            var notification = {
                data:{
                    title:'Some one sent an message',
                    body:msgBody
                },
                token:snapshot.val()
            };
            adminRTDB.messaging().send(notification).then((res:any)=>{
                console.log('Notification sent succefully.',res)
            }).catch((err:any)=>{
                console.log('Notification sent failed.',err)
            })
        },function(err:any){
            console.log('err1',err);
        });
    })
