import { Component } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { AngularFireAuth } from '../../node_modules/@angular/fire/auth';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { UserInfoComponent } from './user-info/user-info.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'spam-me';
  constructor(public afAuth: AngularFireAuth,public  authService:  AuthService,public dialog: MatDialog){

  }

  openUserInfo() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.minWidth = '600px';
    
    const dialogRef = this.dialog.open(UserInfoComponent,dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
