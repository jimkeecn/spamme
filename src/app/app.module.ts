import { NgModule } from '@angular/core';


import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/* Firebase */
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule,AngularFireDatabase } from '@angular/fire/database';

import { AngularFireAuth } from '../../node_modules/@angular/fire/auth';
import { AngularFirestore } from '../../node_modules/@angular/fire/firestore';

import { AngularFireMessaging,AngularFireMessagingModule } from '../../node_modules/@angular/fire/messaging';

/* Firebase End */

/* Angular Material */
import {MatButtonModule, MatCheckboxModule,MatDialogModule,
  MatInputModule,MatSelectModule, MatProgressSpinnerModule} from '@angular/material';
/* Angular Material End */

import { DashboardComponent } from './core/dashboard/dashboard.component';
import { LdsRollerComponent } from './shared/lds-roller/lds-roller.component';
import { SideNavComponent } from './core/side-nav/side-nav.component';
import { MainContentComponent } from './core/main-content/main-content.component';
import { CounterSideComponent } from './core/counter-side/counter-side.component';
import { environment } from '../environments/environment';

import { AuthService } from 'app/services/auth.service';
import { UserInfoComponent } from './user-info/user-info.component';
import { CenterService } from './services/central.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    LdsRollerComponent,
    SideNavComponent,
    MainContentComponent,
    CounterSideComponent,
    UserInfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireMessagingModule,

    MatButtonModule, MatCheckboxModule,MatDialogModule,MatInputModule,MatSelectModule,MatProgressSpinnerModule
  ],
  providers: [AuthService,AngularFireAuth,AngularFirestore,AngularFireMessaging,AngularFireDatabase],
  bootstrap: [AppComponent],
  entryComponents:[UserInfoComponent]
})
export class AppModule { }
