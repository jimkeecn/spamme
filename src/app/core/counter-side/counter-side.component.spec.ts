import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterSideComponent } from './counter-side.component';

describe('CounterSideComponent', () => {
  let component: CounterSideComponent;
  let fixture: ComponentFixture<CounterSideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterSideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
