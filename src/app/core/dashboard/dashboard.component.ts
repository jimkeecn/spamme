import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '../../../../node_modules/@angular/fire/auth';
import { AuthService } from '../../services/auth.service';
import { MessagingService } from '../../services/messaging.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  message;
  constructor(public afAuth: AngularFireAuth,public  authService:  AuthService,
    public msgService: MessagingService) { }

    ngOnInit() {
      this.msgService.getPermission()
      this.msgService.receiveMessage()
      this.message = this.msgService.currentMessage
    }
  
}
