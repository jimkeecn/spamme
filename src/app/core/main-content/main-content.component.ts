import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  message:string = "";
  selectedPerson:string = "All";
  constructor(public authService : AuthService) { }

  ngOnInit() {
  }

  clearMsg(){
    this.message = null;
  }
}
