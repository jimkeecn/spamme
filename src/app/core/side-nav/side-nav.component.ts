import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { spammeUser } from '../../models/user';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  constructor(public authService : AuthService) { }

  ngOnInit() {
  }

  messageTo(user:spammeUser){
    this.authService.whoAmISpeakingto = user;
  }
}
