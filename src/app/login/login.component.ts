import { Component, OnInit } from '@angular/core';
import {moveIn ,fallIn} from '@spamme-animations/routing-animation'
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
   animations:[moveIn()],
   host:{'[@moveIn]':''}
})
export class LoginComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth,public  authService:  AuthService) { }

  ngOnInit() {
  }


}
