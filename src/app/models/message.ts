import { Timestamp } from '../../../node_modules/rxjs';

export class message{
    id?:string;
    recipient?:string;
    recipientName?:string;
    owner?:string;
    ownerName?:string;
    content?:string;
    urgency?:'high' | 'normal' | 'low' | null;
    sendAt?:Date;
    isCompleted?:boolean;
}