import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { spammeUser } from '../models/user';
import { map, first } from 'rxjs/operators';
import { USER_LIST, USER_STORE, CURRENT_USER} from '@spamme-constants/localstorage';
import { USER_COLLECTION, MESSAGE_COLLECTION } from '@spamme-constants/dbCollections';
import { Subject, Observable, interval } from '../../../node_modules/rxjs';
import { message } from '../models/message';
import { promise } from '../../../node_modules/protractor';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  spammeUser : spammeUser;
  isLogin : boolean = false;
  loginUserAvatar:string;
  spammeUserList : spammeUser[] = [];
  
  /* Seperate them*/

  /* auto Log off */
  CHECK_INTERVAL : number = 5000; // in ms
  MINUTES_UNITL_AUTO_LOGOUT = 20 // in mins
  STORE_KEY =  'lastAction';
  get lastAction() {
    return parseInt(localStorage.getItem(this.STORE_KEY));
  }
  set lastAction(value) {
    localStorage.setItem(this.STORE_KEY, value.toString());
  }
  reset() {
    this.lastAction = Date.now();
  }
  initInterval() {
    setInterval(() => {
      this.checkUserActivateStatus();
    }, this.CHECK_INTERVAL);
  }
  initListener() {
    document.body.addEventListener('click', () => this.reset());
  }
  public checkUserActivateStatus(){
    const now = Date.now();
    const timeleft = this.lastAction + this.MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    if (isTimeout && this.isLoggedIn) {
      console.log('you have been auto logout......');
      this.logout();
    }
  }


  /* auto Log off End */

  public getOwnerName(uid:string){
    var name : string = "";
    this.spammeUserList.forEach(user=>{
      if(user.id === uid){
        name = user.userName;
      }
    })
    return name;
  }

  public getAvatar(uid:string){
    var thing : string = "";
    this.spammeUserList.forEach(user=>{
      if(user.id === uid){
        thing = user.userAvatar;
      }
    })
    return thing;
  }

  public getOnlineStatus(uid:string){
    var status:number = null;
    this.spammeUserList.forEach(user=>{
      if(user.id === uid){
        status = user.userStatus;
      }
    })
    return status;
  }

  public getNormalDateFormat(date:Date){
    return date.toLocaleString();
  }

  whoAmISpeakingto : spammeUser;
  messagesList : message[] = [];
  userLoader : boolean = false;
  coreLoader : boolean = true;
  myMessageList : any[] = [];
  public getMyMessages(){
    debugger;
    var promise = new Promise((resolve,reject) =>{
      this.db.collection(USER_COLLECTION).doc(this.user.uid).
      collection(MESSAGE_COLLECTION,ref => ref.where('owner', '==', this.whoAmISpeakingto.id )).snapshotChanges()
      .pipe(map(messageArray=>{
        debugger;
        return messageArray.map((message:any)=>{
          return {
            id: message.payload.doc.id,
            owner:message.payload.doc.data().owner,
            ownerName:message.payload.doc.data().ownerName,
            content:message.payload.doc.data().content,
            urgency:message.payload.doc.data().urgency,
            sendAt:message.payload.doc.data().sendAt.toDate()
          }
        })
      })).subscribe(res=>{
         resolve(this.myMessageList = res);
      })
    })
    return promise;
  }

  public messageTo(user:spammeUser){
    this.coreLoader = true;
    this.whoAmISpeakingto = user;
    this.db.collection(USER_COLLECTION).doc(this.whoAmISpeakingto.id).collection(MESSAGE_COLLECTION)
    .snapshotChanges().pipe(map(messageArray=>{
      return messageArray.map((message:any)=>{
        return {
          id: message.payload.doc.id,
          owner:message.payload.doc.data().owner,
          ownerName:message.payload.doc.data().ownerName,
          content:message.payload.doc.data().content,
          urgency:message.payload.doc.data().urgency,
          sendAt:message.payload.doc.data().sendAt.toDate(),
          recipient:message.payload.doc.data().recipient
        }
      })
    })).subscribe(res=>{
      res.sort((a:message,b:message) =>{
        if(a.sendAt > b.sendAt) return -1;
        if(a.sendAt < b.sendAt) return 1;
        return 0;
      })
      this.messagesList = res.filter(msg=>{
        return msg.owner == this.getCurrentUserID() || msg.owner == this.whoAmISpeakingto.id;
      });
      
      setTimeout(()=>{
        console.log(this.messagesList);
        this.coreLoader = false;
      },500)
    });
  }

  messageTobeSent : message = {};
  messageOnLoader : boolean = false;
  public sendMessage(content:string){
    this.messageOnLoader = true;
    if(content){
      console.log(content);

      this.messageTobeSent = {
        owner:this.getCurrentUserID(),
        ownerName:this.spammeUser.userName,
        content:content,
        urgency:'normal',
        sendAt:new Date(),
        recipient:this.whoAmISpeakingto.id
      };
      this.db.collection(USER_COLLECTION).doc(this.whoAmISpeakingto.id).collection(MESSAGE_COLLECTION).add(this.messageTobeSent).then(res=>{
        console.log(res);
        this.messageOnLoader = false;
      }).catch(err=>{
        console.log(err);
      })
      this.db.collection(USER_COLLECTION).doc(this.user.uid).collection(MESSAGE_COLLECTION).add(this.messageTobeSent).then(res=>{
        console.log(res);
        this.messageOnLoader = false;
      }).catch(err=>{
        console.log(err);
      })
    }else{
      this.messageOnLoader = false;
      return;
    }
   
  } 
  /* Seperate them End*/

  constructor(public afAuth: AngularFireAuth, public router: Router,public db: AngularFirestore,) {
    this.userLoader = true;
    this.checkUser();
    this.getSpammerUserList();
    this.initListener();
    this.initInterval();
  }

  public getSpammerUserList(){
    this.db.collection(USER_COLLECTION).snapshotChanges().pipe(map(userArray => {
      return userArray.map((user:any)=>{
        return {
          id: user.payload.doc.id,
          userName:user.payload.doc.data().userName,
          userAvatar:user.payload.doc.data().userAvatar,
          userStatus:user.payload.doc.data().userStatus,
          userEmail:user.payload.doc.data().userEmail,
        }
      })
      })).subscribe(res=>{
        if(this.isLoggedIn){
          this.userLoader = false;
          localStorage.setItem(USER_LIST,JSON.stringify(res));
          console.log(res);
  
          this.spammeUserList = res.filter(obj=>{
            return obj.id !== this.getCurrentUserID() && obj.userName !== "";
          });
          if(this.whoAmISpeakingto){
            return;
          }else{
            var theArray = res.filter(user=>{
              return user.id !== this.user.uid && user.userName !== "";
            })
            this.whoAmISpeakingto = theArray[0];
            this.messageTo(this.whoAmISpeakingto);
            this.coreLoader = false;
          }
        }
      })
  }

  public getCurrentUserID():string{
    if(JSON.parse(localStorage.getItem(USER_STORE))){
      return JSON.parse(localStorage.getItem(USER_STORE)).uid;
    }else{
      return;
    }
    
  }  

  public checkUser(){
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem(USER_STORE, JSON.stringify(this.user));
        this.router.navigate(['/dashboard']);
        this.isRegisterUser(this.user);
      } else {
        localStorage.setItem(USER_STORE, null);
      }
    })
  }

  private isRegisterUser(currentUser:User){
    this.spammeUser = {
      userName: "",
      userAvatar:"",
      userStatus:1,
      userEmail:currentUser.email,
    }
    this.db.collection(USER_COLLECTION).doc(currentUser.uid).ref.get().then(doc=>{
      if(doc.data()){
        var foundUser = doc.data();
        this.loginUserAvatar = foundUser.userAvatar;
        localStorage.setItem(CURRENT_USER, JSON.stringify(foundUser));
        console.log('Found User');
        return;
      }else{
        this.db.collection(USER_COLLECTION).doc(currentUser.uid).set(this.spammeUser).then(res=>{
          console.log('Create User',res)
          localStorage.setItem(CURRENT_USER, JSON.stringify(this.spammeUser));
          return res;
        },err =>{
          console.log(err)
          return err;
        })
      }
    })
  }

  async  login(email: string, password: string) {
    try {
      this.isLogin = true;
      await this.afAuth.auth.signInWithEmailAndPassword(email, password)
      var currentUser : spammeUser = {
        userStatus : 1
      }
      this.db.collection(USER_COLLECTION).doc(this.afAuth.auth.currentUser.uid).update(currentUser).then(res=>{
        this.getSpammerUserList();
        this.router.navigate(['/dashboard']);
      })
    } catch (e) {
      this.isLogin = false;
      localStorage.removeItem(CURRENT_USER);
      alert("Error!" + e.message);
    }
  }

  async logout() {
    var currentUser : spammeUser = {
      userStatus : 2
    }
    this.db.collection(USER_COLLECTION).doc(this.afAuth.auth.currentUser.uid).update(currentUser).then(res=>{
      this.afAuth.auth.signOut();
      this.isLogin = false;
      localStorage.removeItem(USER_STORE);
      localStorage.removeItem(CURRENT_USER);
      localStorage.removeItem(USER_LIST);
      this.router.navigate(['/login']);
    })
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem(USER_STORE));
    return user !== null;
  }


}
