import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { spammeUser } from '../models/user';
import { map, first } from 'rxjs/operators';
import { USER_LIST, USER_STORE, CURRENT_USER} from '@spamme-constants/localstorage';
import { USER_COLLECTION } from '@spamme-constants/dbCollections';
import { AuthService } from './auth.service';
@Injectable({
    providedIn: 'root'
})
export class CenterService {
  userList : spammeUser[];
  whoAmIspeakingTo : spammeUser;
  

  constructor( public authService : AuthService){

   }
}