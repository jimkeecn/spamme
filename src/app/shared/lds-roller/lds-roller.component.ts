import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lds-roller',
  template: `
  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
  `,
  styleUrls: ['./lds-roller.component.scss']
})
export class LdsRollerComponent {

  constructor() { }

  ngOnInit() {
  }

}
