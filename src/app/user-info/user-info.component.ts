import { Component, OnInit } from '@angular/core';
import { spammeUser } from '../models/user';
import { MatDialogRef } from '@angular/material';
import { UserService } from './user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  currentUser : spammeUser;
  isSaving : boolean = false;
  constructor( public dialogRef: MatDialogRef<UserInfoComponent>, public userService : UserService, public authService : AuthService) { 
    this.currentUser = JSON.parse(localStorage.getItem('current-user'));
  }

  ngOnInit() {
    console.log(this.currentUser);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveUserInfo() :void {
    this.isSaving = true;
    this.userService.saveUserInfo(this.currentUser).then(res=>{
      console.log('User Info Update Successed',res);
      this.authService.checkUser();
      this.dialogRef.close();
    },err=>{
      console.error(err);
      alert('User Info Update Failed');
      this.dialogRef.close();
    })
  }
}
