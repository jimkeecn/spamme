import { Injectable } from '@angular/core';
import { spammeUser } from '../models/user';
import { AngularFireAuth } from '../../../node_modules/@angular/fire/auth';
import { Router } from '../../../node_modules/@angular/router';
import { AngularFirestore } from '../../../node_modules/@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(public afAuth: AngularFireAuth, public router: Router,public db: AngularFirestore) { }
  
  public saveUserInfo(spammeUser:spammeUser){
    return new Promise<any>((resolve, reject) => {
      var currentUser = JSON.parse(localStorage.getItem('user'));
      this.db.collection('user').doc(currentUser.uid).update(spammeUser).then(res=>{
        console.log('Create User',res)
        resolve(res);
      },err =>{
        console.log(err)
        reject(err);
      })
    })
  }
}
